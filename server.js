import express from 'express';
import { graphqlExpress,  graphiqlExpress } from 'graphql-server-express';
import bodyParser from 'body-parser';
import { schema } from './src/schema';
import cors from 'cors';

const PORT = 4000;

const server = express();

const whitelist = ['http://localhost:4000', 'http://localhost:8080', 'http://cms.esports-scores.com']
const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error(origin + ' Not allowed by CORS'))
    }
  }
}

server.use('*', cors(corsOptions) );

server.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql'
}));

server.use('/graphql', bodyParser.json(), graphqlExpress({
  schema
}));;

server.listen(PORT, () => console.log(`GraphQL Server is now running on http://localhost:${PORT}`));
