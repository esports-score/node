import mongoose, { Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import assert from 'assert';

// Mongoose Schema definition
const TeamsSchema = new Schema({

  country: String,
  teamTag: String,
  logo: String,
  matches: Schema.Types.Mixed,
  name: String,
  players: Schema.Types.Mixed,
  steamId: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  url: String,
  tournaments: Schema.Types.Mixed
});

TeamsSchema.plugin(timestamps);
TeamsSchema.index({ teamTag: 'text', country: 'text', name: 'text' });
export const TeamsDB = mongoose.model( 'Teams', TeamsSchema );

