import mongoose, { Schema } from 'mongoose';
import assert from 'assert';

const dayInMs = 1000 * 60 * 60 * 24 * 2; // 2 days

const LoggingSchema = new Schema({
  createdAt: {
    type: Date,
    default: Date.now,
    expires: dayInMs
  },
  type: {
    type: String,
    required: true,
    trim: true,
    default: 'Error'
  },
  modual: {
    type: String,
    required: true,
    trim: true
  },
  note: {
    type: String,
    required: true,
    trim: true
  },
  text: {
    type: String,
    required: true,
  }
});

export const LoggingDB = mongoose.model( 'Logging', LoggingSchema );