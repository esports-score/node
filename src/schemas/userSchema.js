import mongoose, { Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import assert from 'assert';

const UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  googleId: {
    type: String,
    required: true,
  },
  googleInfo: {
    type: Schema.Types.Mixed,
    required: true,
  }
});

MatchesSchema.plugin(timestamps);
export const UserDB = mongoose.model( 'Users', UserSchema );