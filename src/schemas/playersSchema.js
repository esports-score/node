import mongoose, { Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import assert from 'assert';

// Mongoose Schema definition
const PlayerSchema = new Schema({
  active: {
    type: Boolean,
    default: true
  },
  captain: {
    type: Boolean,
    default: false
  },
  activeTeam: String,
  birthDate: Date,
  country: String,
  firstName: String,
  lastName: String,
  fullName: String,
  nickName: String,
  picture: String,
  position: String,
  steamId: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  accountId: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  twitch: String,
  social: Schema.Types.Mixed,
  matches: Schema.Types.Mixed,
  teams: Schema.Types.Mixed,
  tournaments: Schema.Types.Mixed
});
PlayerSchema.plugin(timestamps);
PlayerSchema.index({ activeTeam: 'text', country: 'text', firstName: 'text', lastName: 'text', nickName: 'text' });
export const PlayerDB = mongoose.model( 'Players', PlayerSchema );