import mongoose, { Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import assert from 'assert';

// Mongoose Schema definition
export const TournamentsSchema = new Schema({
  description: String,
  startDate: String,
  endDate: String,
  name: String,
  organizer: String,
  production: String,
  results: String,
  leagueid: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  relatedTournaments: Schema.Types.Mixed,
  itemdef: String,
  league_tier: String,
  tournament_url: String,
  description : String,
  format: Schema.Types.Mixed,
  teams: Schema.Types.Mixed,
  players: Schema.Types.Mixed,
  matches: Schema.Types.Mixed
});
TournamentsSchema.plugin(timestamps);
TournamentsSchema.index({ description: 'text', name: 'text', organizer: 'text' });
export const TournamentsDB = mongoose.model( 'Tournaments', TournamentsSchema );
