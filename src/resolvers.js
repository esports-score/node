import mongoose, { Schema } from 'mongoose';
import assert from 'assert';
import { MatchesDB } from './schemas/matchesSchema';
import { PlayerDB } from './schemas/playersSchema';
import { TeamsDB } from './schemas/teamsSchema';
import { TournamentsDB } from './schemas/tournamentsSchema';
import { LoggingDB } from './schemas/loggingSchema';
import GraphQLJSON from 'graphql-type-json';

const COMPOSE_URI_DEFAULT = 'mongodb://localhost:27017/dota';

mongoose.Promise = require('bluebird');
mongoose.connect(COMPOSE_URI_DEFAULT, {
  useMongoClient: true
})
.then( ( db ) => {
  console.log('mongo connected')
  //console.log(db)
})
.catch( ( error ) => {
  console.error(error)
})


const dbLookUp = ( db, newQuery ) => {
  return new Promise( ( resolve, reject ) => {
    let QueryTest = db.find(
      { $or : newQuery },
      { score : { $meta: "textScore" } }
    )
    .sort({ score : { $meta : 'textScore' } })
    mongoose.Promise = require('bluebird');
    assert.equal(QueryTest.exec().constructor, require('bluebird'));
    // A query is not a fully-fledged promise, but it does have a `.then()`.
    QueryTest.then( (result) => {
      resolve(result);
    })
    .catch( (error) => {
      reject(error);
    })
  })
}

const dbFindAll = ( db ) =>
{
  return new Promise( (resolve, reject) => {
    db.find( (err, data) =>{
      err ? reject(err) : resolve(data)
    })
  });
}

const findXforX = (db, values, key, length, keyReplace ) => {
  return new Promise( ( resolve, reject ) => {
    if ( key > (length - 1) )
    {
      resolve( values )
    }

    if ( values[key][keyReplace] !== undefined ){
      dbLookUp( db, values[key][keyReplace] )
      .then( (result) => {
        values[key][keyReplace] = result
        key++
        resolve( findXforX( db, values, key, length, keyReplace ) )
      })
    }
    else
    {
      key++
      resolve( findXforX( db, values, key, length, keyReplace ) )
    }
  })
}

const deleteEntry = ( db, id ) => {
  return new Promise( ( resolve, reject ) => {
    let DeleteEntry = db.deleteOne( {_id: mongoose.Types.ObjectId( id ) })

    mongoose.Promise = require('bluebird');
    assert.equal(DeleteEntry.exec().constructor, require('bluebird'));
    // A query is not a fully-fledged promise, but it does have a `.then()`.
    DeleteEntry.then( (result) => {
      resolve( id );
    })
    .catch( (error) => {
      reject(error);
    })
  })
}

export const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    //All Players
    players: () => {
      return new Promise( (resolve, reject) => {
        PlayerDB.find( (err, players) => {
          err ? reject(err) : resolve(players)
        })
      })
    },

    //All Teams
    team: () => {
      return new Promise( (resolve, reject) => {
        dbFindAll( TeamsDB )
        .then( values => {

          resolve( findXforX( PlayerDB, values, 0, values.length, 'players' ) )

        })
      })
    },

    //All Tournaments
    tournaments: () => {

      return new Promise( (resolve, reject) => {
        dbFindAll( TournamentsDB )
        .then( values => {
          return findXforX( PlayerDB, values, 0, values.length, 'players' )
        })
        .then( values => {
          resolve( findXforX( TeamsDB, values, 0, values.length, 'teams' ) )
        })
      })

    },

    //All Matches
    matches: () => {
      return new Promise( (resolve, reject) => {
        MatchesDB.find( (err, matches) => {
          err ? reject(err) : resolve(matches)
        })
      });
    },

    //All Logging
    logging: () => {
      return new Promise( (resolve, reject) => {
        LoggingDB.find( (err, loggs) => {
          err ? reject(err) : resolve(loggs)
        })
      });
    },


    //find Player
    findPlayer: (root, args) =>{

      const newQuery = new RegExp( args.query, 'i' )

      return dbLookUp ( PlayerDB, [ { nickName: newQuery }, { fullName: newQuery }, {activeTeam: newQuery}, {country: newQuery}, ] )
    },

    //search Player
    searchPlayer: (root, args) => {

      let newQuery = {}
      Object.keys(args.input).forEach(function(key)
      {
        newQuery[key] = new RegExp( args.input[key], 'i' )
      });
      return dbLookUp ( PlayerDB, [ newQuery ] )
    },

    //find findTeam
    findTeam: (root, args) =>{

      const newQuery = new RegExp( args.query, 'i' )
      return dbLookUp ( TeamsDB, [ { teamTag: newQuery }, { country: newQuery }, {name: newQuery} ] )
    },

    //search Team
    searchTeam: (root, args) =>{

      let newQuery = {}
      Object.keys(args.input).forEach(function(key)
      {
        newQuery[key] = new RegExp( args.input[key], 'i' )
      });
      return new Promise( (resolve, reject) => {
        dbLookUp( TeamsDB, [ newQuery ] )
        .then( values => {

          resolve( findXforX( PlayerDB, values, 0, values.length, 'players' ) )
        })
      })
    },

    //find Tournament
    findTournament: (root, args) =>{

      const newQuery = new RegExp( args.query, 'i' )
      return new Promise( (resolve, reject) => {
        dbLookUp ( TournamentsDB, [ { name: newQuery }, { organizer: newQuery }, {production: newQuery} ] )
        .then( values => {
          return findXforX( PlayerDB, values, 0, values.length, 'players' )
        })
        .then( values => {
          resolve( findXforX( TeamsDB, values, 0, values.length, 'teams' ) )
        })
      })
    },

    //search Tournament
    searchTournament: (root, args) =>{
      let newQuery = {}
      Object.keys(args.input).forEach(function(key)
      {
        newQuery[key] = new RegExp( args.input[key], 'i' )
      });
      return new Promise( (resolve, reject) => {
        dbLookUp( TournamentsDB, [ newQuery ] )
        .then( values => {
          return findXforX( PlayerDB, values, 0, values.length, 'players' )
        })
        .then( values => {
          resolve( findXforX( TeamsDB, values, 0, values.length, 'teams' ) )
        })
      })
    },

    //Find Matches
    findMatches: (root, args) =>{
      return new Promise( (resolve, reject) => {
        let QueryTest = MatchesDB.find(
          { $text : { $search : args.query } },
          { score : { $meta: "textScore" } }
        )
        .sort({ score : { $meta : 'textScore' } })
        mongoose.Promise = require('bluebird');
        assert.equal(QueryTest.exec().constructor, require('bluebird'));
        // A query is not a fully-fledged promise, but it does have a `.then()`.
        QueryTest.then( (doc) => {
          resolve(doc);
        });
      })
    }
  },

  Mutation: {
    //add Player
    addPlayer: (root, args) => {
      const newPlayer = new PlayerDB ({
        ...args.input
      });

      newPlayer.id = PlayerDB._id;
      return new Promise((resolve, reject) => {
        newPlayer.save(function (err) {
          err ? reject(err) : resolve(newPlayer)
        })
      })
    },

    //add Tournament
    addTournament: (root, args) => {

      const newTournament = new TournamentsDB ({
        ...args.input
      });

      newTournament.id = TournamentsDB._id;

      return new Promise((resolve, reject) => {
        newTournament.save(function (err) {
          err ? reject(err) : resolve(newTournament)
        })
      })
    },

    //add Team
    addTeam: (root, args) => {
      const newTeam = new TeamsDB ({
        ...args.input
      });
      newTeam.id = TeamsDB._id;

      const playersToFind = args.input.players
      const LookUpPlayer = dbLookUp ( PlayerDB, playersToFind )

      const SaveTeam = new Promise((resolve, reject) => {
        newTeam.save(function (err) {
          err ? reject(err) : resolve(newTeam)
        })
      })

      return new Promise((resolve, reject) => {
        Promise.all( [LookUpPlayer, SaveTeam] )
        .then(values => {
          let test = values[1]
          test.players = values[0]
          resolve(test)
        })
        .catch(reason => {
          reject(reason)
        })
      })
    },


    //add Match
    addMatch: (root, args) => {
      const newMatch = new MatchesDB ({
        ...args.input
      });
      newMatch.id = MatchesDB._id;
      return new Promise((resolve, reject) => {
        newMatch.save(function (err) {
          err ? reject(err) : resolve(newMatch)
        })
      })
    },
    //Update by ID
    updatePlayer: (root, args) => {
      const id = args.id;
      return new Promise((resolve, reject) => {
        PlayerDB.findByIdAndUpdate(args.id, { $set: { ...args.input }}, { new: true }, function (err, player) {
          err ? reject(err) : resolve(player)
        })
      })
    },

    //Update by ID
    updateTeam: (root, args) => {

      const playersToFind = args.input.players

      const LookUpPlayer = dbLookUp ( PlayerDB, playersToFind )

      const updateTeam = new Promise((resolve, reject) => {
        TeamsDB.findByIdAndUpdate(args.id, { $set: { ...args.input }}, { new: true }, function (err, team) {
          err ? reject(err) : resolve(team)
        })
      })

      return new Promise((resolve, reject) => {
        Promise.all( [ LookUpPlayer, updateTeam ] )
        .then(values => {
          let test = values[1]
          test.players = values[0]
          resolve(test)
        })
        .catch(reason => {
          reject(reason)
        })
      })
    },

    //Update by ID
    updateMatch: (root, args) => {
      const id = args.id;
      return new Promise((resolve, reject) => {
        MatchesDB.findByIdAndUpdate(args.id, { $set: { ...args.input }}, { new: true }, function (err, match) {
          err ? reject(err) : resolve(match)
        })
      })
    },

    //Update by ID
    updateTournament: (root, args) => {

      const id = args.id;

      return new Promise((resolve, reject) => {
        TournamentsDB.findByIdAndUpdate(args.id, { $set: { ...args.input }}, { new: true }, function (err, tournament) {
          err ? reject(err) : resolve(tournament)
        })
      })

    },

    //Delete by ID
    deletePlayer: (root, args) => {
      const id = args.id;

      return deleteEntry( PlayerDB, id )
    },
    deleteTeam: (root, args) => {
      const id = args.id;

      return deleteEntry( TeamsDB, id )
    },

    deleteMatch: (root, args) => {
      const id = args.id;

      return deleteEntry( MatchesDB, id )
    },

    deleteTournament: (root, args) => {
      const id = args.id;

      return deleteEntry( TournamentsDB, id )
    },
  }
};