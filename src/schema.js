import { makeExecutableSchema } from 'graphql-tools';
import { resolvers } from './resolvers';
import GraphQLJSON from 'graphql-type-json';

const typeDefs = `
scalar JSON

type Logging {
  id: ID!
  createdAt: String!
  type: String!
  modual: String!
  note: String!
  text: String!
}

type Players {
  createdAt: String!
  updatedAt: String!
  nickName: String!
  active: Boolean
  captain: Boolean
  activeTeam: String
  birthDate: String
  country: String
  firstName: String
  id: ID!
  lastName: String
  picture: String
  position: String
  steamId: String
  accountId: String!
  twitch: String
  matches: [Matches] @relation(name: "MatchesOnPlayers")
  teams: [Teams] @relation(name: "PlayersOnTeams")
  tournaments: [Tournaments] @relation(name: "PlayersOnTournaments")
}

type Teams {
  country: String
  createdAt: String!
  updatedAt: String!
  id: ID!
  logo: String
  matches: [Matches] @relation(name: "MatchesOnTeams")
  name: String
  teamTag: String
  players: [Players] @relation(name: "PlayersOnTeams")
  steamId: String!
  url: String
  tournaments: [Tournaments] @relation(name: "TeamsOnTournaments")
}

type Matches {
  createdAt: String!
  dateUTC: String!
  ended: Boolean
  id: ID!
  lenght: String
  matchDetails: JSON
  match_id: String
  players: [Players] @relation(name: "MatchesOnPlayers")
  teams: [Teams] @relation(name: "MatchesOnTeams")
  tournament: Tournaments @relation(name: "MatchesOnTournaments")
  updatedAt: String!
  winner: String
}

type Tournaments {
  id: ID!
  createdAt: String!
  updatedAt: String!
  startDate: String
  endDate: String
  name: String!
  description: String
  tournament_url: String!
  format: JSON
  itemdef: Int
  leagueid: String
  league_tier: String
  production: String
  organizer: String
  relatedTournaments: [Tournaments]
  matches: [Matches] @relation(name: "MatchesOnTournaments")
  players: [Players] @relation(name: "PlayersOnTournaments")
  results: String
  teams: [Teams] @relation(name: "TeamsOnTournaments")
}


input PlayersId{
  accountId: String!
}

input TeamsId{
  steamId: String!
}

input MatchesId{
  match_id: String!
}

input TournamentsId{
  leagueid: String!
}

input PlayersInput{
  nickName: String!
  firstName: String!
  lastName: String!
  active: Boolean
  activeTeam: String
  captain: Boolean
  birthDate: String
  country: String!
  picture: String
  position: String
  steamId: String
  accountId: String
  twitch: String
  matches: [MatchesId]
  teams: [TeamsId]
  tournaments: [TournamentsId]
}

input PlayersSearch{
  id: ID
  nickName: String
  firstName: String
  lastName: String
  active: Boolean
  activeTeam: String
  captain: Boolean
  birthDate: String
  country: String
  position: String
  steamId: String
  accountId: String
  twitch: String
  matches: [MatchesId]
  teams: [TeamsId]
  tournaments: [TournamentsId]
}

input TeamsInput {
  country: String
  logo: String
  matches: [MatchesId]
  name: String
  teamTag: String
  players: [PlayersId]
  steamId: String
  url: String
  tournaments: [TournamentsId]
}

input TeamSearch {
  id: ID
  country: String
  logo: String
  name: String
  teamTag: String
  steamId: String
  url: String
}

input MatchesInput {
  dateUTC: String!
  ended: Boolean
  lenght: String
  matchDetails: JSON
  matchId: String
  winner: String
  tournament: String
  players: [PlayersId]

}

input TournamentSearch {
  id: ID
  description: String
  endDate: String
  startDate: String
  leagueid: String
  league_tier: String
  production: String
  organizer: String
  name: String
}

input TournamentsInput {
  description: String
  endDate: String
  itemdef: Int
  leagueid: String!
  format: JSON
  league_tier: String
  production: String
  organizer: String
  matches: [MatchesId]
  relatedTournaments: [TournamentsId]
  name: String!
  players: [PlayersId]
  results: String
  startDate: String
  teams: [TeamsId]
  tournament_url: String!
}

type Query {
  players : [Players]
  findPlayer(query: String!) : [Players]
  searchPlayer(input: PlayersSearch ): [Players]

  team : [Teams]
  findTeam(query: String!) : [Teams]
  searchTeam(input: TeamSearch ): [Teams]

  tournaments : [Tournaments]
  findTournament(query: String!) : [Tournaments]
  searchTournament(input: TournamentSearch ): [Tournaments]

  matches : [Matches]
  findMatches(query: String!) : [Matches]

  logging : [Logging]
}

type Mutation {
  addPlayer(input: PlayersInput ): Players
  updatePlayer(id: ID!, input: PlayersInput ): Players
  deletePlayer(id: ID!): ID

  addTeam(input: TeamsInput ) : Teams
  updateTeam(id: ID!, input: TeamsInput) : Teams
  deleteTeam(id: ID!): ID

  addMatch(input: MatchesInput ) : Matches
  updateMatch(id: ID!, input: MatchesInput) : Matches
  deleteMatch(id: ID!): ID

  addTournament(input: TournamentsInput ) : Tournaments
  updateTournament(id: ID!, input: TournamentsInput) : Tournaments
  deleteTournament(id: ID!): ID
}
`;

export const schema = makeExecutableSchema({ typeDefs, resolvers });
